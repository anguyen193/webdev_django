0. git clone https://andy193@bitbucket.org/andy193/webdev_django.git
1. Make a new virtualenv for finder, e.g: finder_env and activate it
2. cd finder
3. pip install -r requirements/dev.txt
4. cp finder/settings/local.sample.py finder/settings/local.py
5. Change local.py to reflect your local settings
6. python manage.py migrate
7. python manage.py createsuperuser
8. python manage.py runserver
9. Visit http://localhost:8000/admin/ and set default example site's domain to yourdomain (localhost:8000 for dev)
10. Start coding!
