$(document).ready(function() {
    var validator = $('#search_form').bootstrapValidator({
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            name: {
                message: 'required',
                validators: {
                    notEmpty: {
                        message: 'The name is required'
                    },
                    regexp: {
                        regexp: /^[a-zA-Zöä ]+$/,
                        message: 'The name must contain only alphabetical and whitespace'
                    },
                    stringLength: {
                        max: 30,
                        message: 'The name must be less than 30 characters'
                    }
                }
            },
            depth: {
                validators: {
                    notEmpty: {
                        message: 'The depth is required'
                    },
                }
            }
        }
    });
    $('#button').click(function(e) {
    // validator.on('success.form.fv', function(e) {
        e.preventDefault();
        $(document).ajaxSend(function(event, request, settings) {
            $('#loading-indicator').show();
        });
        $.ajax({
            url: '/finder/',
            type: 'POST',
            dataType: 'json',
            data: {name: $('#name').val(), depth: $('#depth').val()}

        }).done( function(resp) {
            $(document).ajaxComplete(function(event, request, settings) {
              $('#loading-indicator').hide();
            });

            console.log(resp['results']);
            // create an array with nodes
            var nodes = new vis.DataSet();

            // create an array with edges
            var edges = new vis.DataSet();

            var mock = resp['results'];
            var mock_arr = [mock];


            var nodes_array = [];
            var edges_array = [];

            function look_up_key(key, arr, group, companies_name) {
                for (var i = 0; i < arr.length; i++) {
                    if (arr[i].id === key) {
                        if (group > arr[i].group) {
                            arr[i].group = group;
                            arr[i].title = companies_name;
                        }
                        return true;
                    }
                }
                return false;
            }

            function look_up_pair(key, child_key, arr) {
                for (var i = 0; i < arr.length; i++) {
                    if (arr[i].from === key && arr[i].to === child_key ||
                        arr[i].to === key && arr[i].from === child_key)
                        return true;
                }
                return false;
            }

            function iterateThroughData(arr, depth) {
                    
                for (var i = 0; i < arr.length; i++) {
                    var key = arr[i]['name'];
                    var companies_name = arr[i]['companies'];

                    if (! look_up_key(key, nodes_array, depth, companies_name)) {
                        nodes_array.push({
                            id: key,
                            label: key,
                            title: companies_name,
                            group: depth

                        });
                    }

                    for (var j = 0; j < arr[i]['connections'].length; j++) {

                        var child_key = arr[i]['connections'][j]['name'];
                            
                        if (!look_up_pair(key, child_key, edges_array)) {
                            edges_array.push({from : key, to : child_key});
                        }
                    }
                    iterateThroughData(arr[i]['connections'], depth - 1);
                }
            }
            if (mock_arr[0].length == 0) {
                $('#message').text("Error: Invalid input. Please try again!");
                $('#message').css("color", "red");
                $("#message").show().delay(5000).fadeOut();
            } else if (!('connections' in mock_arr[0])) {
                $('#message').text("Did you mean: " + mock_arr[0] + "?");
                $('#message').css("color", "red");
                $("#message").show().delay(10000).fadeOut();
            
            } else {
                iterateThroughData(mock_arr, Number($('#depth').val()));
            }
            var container = document.getElementById('response');

            console.log(nodes);
            edges.add(edges_array);
            nodes.add(nodes_array);

            // provide the data in the vis format
            var data = {
                nodes: nodes,
                edges: edges
            };

            var options = {
                nodes: {
                    shape: 'dot',
                    font: {
                      size: 20,
                      face: 'Tahoma'
                    },
                },
                edges: {
                    physics: false
                },
                interaction: {
                    hideEdgesOnDrag: true,
                    tooltipDelay: 200,
                    hover: true
                },
                layout: {
                    // hierarchical: true,
                    // improvedLayout: false
                }
            };

            // initialize your network!
            var network = new vis.Network(container, data, options);

            console.log(network);
        });
    });
});
