from finder.settings.configs import *

# Make this unique, and don"t share it with anybody.
SECRET_KEY = "56$8g^39f3-8gi523+5&9a$%1p79dnm2hf+0s+j6m+pc)$-ksa"

DATABASES = {
    "default": {
        "ENGINE": "django.db.backends.mysql",
        "NAME": "finder",
        "USER": "finder",
        "PASSWORD": "finder_PASS",
        "HOST": "localhost",
        "PORT": "",
    }
}

DEBUG = True
TIME_ZONE = "Asia/Ho_Chi_Minh"

EMAIL_BACKEND = "django.core.mail.backends.console.EmailBackend"
