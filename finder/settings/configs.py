# coding=utf-8
from os.path import join, dirname, abspath, sep


ROOT_PATH = join(dirname(dirname(__file__)))

SITE_ID = 1

DEBUG = True

# translation and locale
LANGUAGE_CODE = "en"
ADMIN_LANGUAGE_CODE = "en"
LANGUAGES = [
    ("en", "English"),
]
LOCALE_PATHS = [abspath(join(ROOT_PATH, join("..", "locale"))) + sep]
USE_I18N = True
USE_L10N = True
USE_TZ = False
TIME_ZONE = "UTC"

# media
MEDIA_ROOT = abspath(join(ROOT_PATH, "media"))
MEDIA_URL = "/media/"

# static
STATIC_ROOT = ""
STATIC_URL = "/static/"
STATICFILES_DIRS = (
    abspath(join(ROOT_PATH, "static")),
)

ALLOWED_HOSTS = [
    "localhost", "127.0.0.1", ".finder.com", ".finder.vn"
]

ADMINS = (
    ("Anh Nguyen", "andy193.work@gmail.com"),
)

MANAGERS = ADMINS

# List of finder classes that know how to find static files in
# various locations.
STATICFILES_FINDERS = (
    "django.contrib.staticfiles.finders.FileSystemFinder",
    "django.contrib.staticfiles.finders.AppDirectoriesFinder",
)

TEMPLATES = [
    {
        "BACKEND": "django.template.backends.django.DjangoTemplates",
        "APP_DIRS": True,
        "DIRS": [
            abspath(join(ROOT_PATH, "templates")),
        ],
        "OPTIONS": {
            "context_processors": [
                "django.contrib.auth.context_processors.auth",
                "django.template.context_processors.debug",
                "django.template.context_processors.i18n",
                "django.contrib.messages.context_processors.messages",
                "django.template.context_processors.media",
                "django.template.context_processors.static",
                "django.template.context_processors.request",
            ],
        }
    },
]

MIDDLEWARE_CLASSES = (
    "django.contrib.sessions.middleware.SessionMiddleware",
    "django.middleware.locale.LocaleMiddleware",
    "django.middleware.common.CommonMiddleware",
    "django.middleware.csrf.CsrfViewMiddleware",
    "django.contrib.auth.middleware.AuthenticationMiddleware",
    "django.contrib.messages.middleware.MessageMiddleware",
    "django.middleware.clickjacking.XFrameOptionsMiddleware",
)

ROOT_URLCONF = "finder.urls"

# Python dotted path to the WSGI application used by Django"s runserver.
WSGI_APPLICATION = "finder.wsgi.application"

AUTHENTICATION_BACKENDS = (
    # Needed to login by username in Django admin, regardless of `allauth`
    "django.contrib.auth.backends.ModelBackend",
    # `allauth` specific authentication methods, such as login by e-mail
)

INSTALLED_APPS = (
    "django.contrib.auth",
    "django.contrib.contenttypes",
    "django.contrib.sessions",
    "django.contrib.sites",
    "django.contrib.messages",
    "django.contrib.staticfiles",
    "django.contrib.sitemaps",
    "django.contrib.admin",

    # ----- finder APPS ------ #
    "finder.apps.main.conf.MainConfig",
)

LOGGING = {
    "version": 1,
    "disable_existing_loggers": False,
    "filters": {
        "require_debug_false": {
            "()": "django.utils.log.RequireDebugFalse"
        },
        "require_debug_true": {
            "()": "django.utils.log.RequireDebugTrue"
        }
    },
    "handlers": {
        "mail_admins": {
            "level": "ERROR",
            "filters": ["require_debug_false"],
            "class": "django.utils.log.AdminEmailHandler",
            "include_html": True,
        },
    },
    "loggers": {
        "django.request": {
            "handlers": ["mail_admins"],
            "level": "ERROR",
            "propagate": True,
        },
    }
}
