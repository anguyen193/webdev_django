import json

from annoying.decorators import ajax_request, render_to

from bs4 import BeautifulSoup

from django.views.decorators.csrf import csrf_exempt

import requests

from .models import Finder

headers = {
    'User-Agent': (
        'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_1)'
        ' AppleWebKit/537.36 (KHTML, like Gecko)'
        ' Chrome/39.0.2171.95 Safari/537.36'
    )
}


@render_to('index.html')
def index(request):
    return {}


@ajax_request
@csrf_exempt
def finder(request):
    """
    Network route.

    Expected results:
    results = {
        'name': 'man A',
        'connections': [
            {'name': 'man B', 'connections': []},
            {'name': 'man C', 'connections': [
                {'name': 'man D', 'connections': []},
            ]},
        ]
    }
    """
    results = {}

    if request.method == 'POST':
        depth = request.POST.get('depth')
        form_name = request.POST.get('name')
        depth = int(depth)
        used_names_list = []
        to_fetch_data = Finder.objects.all()

        # first input is root of the dict
        results = {'name': form_name, 'companies': [], 'connections': []}

        # prepare to_loop_next names to loop
        to_loop_next = [results]

        for i in range(depth):
            # prepare to loop names in each depth search
            to_loop_now = to_loop_next

            # reset to_loop_next after each depth
            to_loop_next = []

            # loop member's name for each depth
            for name_dict in to_loop_now:
                name_found = 0
                all_members_list = []

                if name_dict['name'] not in used_names_list:
                    print("name_dict['name']", name_dict['name'])

                    for single_data in to_fetch_data:

                        if name_dict['name'] == single_data.parent_name:
                            decoded_parent_companies = json.loads(
                                single_data.parent_companies
                            )
                            decoded_child_names = json.loads(
                                single_data.child_names
                            )

                            if len(decoded_parent_companies) == 0:
                                print('--------------', decoded_child_names)
                                return {'results': decoded_child_names}
                            else:
                                name_dict['companies'] += decoded_parent_companies
                                innermost_list = name_dict['connections']
                                innermost_list += decoded_child_names
                                to_loop_next += decoded_child_names
                                name_found += 1

                    print(name_found)

                    if name_found == 0:
                        innermost_list = name_dict['connections']
                        companies = _find_company_links(name_dict['name'])
                        companies_name = _find_companies_name(
                            name_dict['name']
                        )
                        name_dict['companies'] += companies_name

                        for company in companies:
                            members = _find_members(company, name_dict['name'])
                            if members is not None:
                                all_members_list += members

                        if name_dict['name'] not in all_members_list:
                            to_store_results = Finder(
                                parent_name=name_dict['name'],
                                child_names=json.dumps(all_members_list),
                                parent_companies=json.dumps([])
                            )
                            to_store_results.save()
                            return {'results': all_members_list}

                        for member in set(all_members_list):

                            if member != name_dict['name']:
                                new_dict = {
                                    'name': member,
                                    'companies': [],
                                    'connections': []
                                }

                                innermost_list.append(new_dict)
                                to_loop_next.append(new_dict)

                        to_store_results = Finder(
                            parent_name=name_dict['name'],
                            child_names=json.dumps(name_dict['connections']),
                            parent_companies=json.dumps(name_dict['companies'])
                        )
                        to_store_results.save()

            for used_name in to_loop_now:
                used_names_list.append(used_name['name'])

    return {'results': results}


def _find_company_links(name):
    """Find companies' links."""
    replaced_link = []

    # Set same charset as finder.fi's charset
    name = name.encode('iso-8859-15', 'ignore')

    payload = {
        'newSearch': 'true',
        'query.searchwords': name,
        'query.searchTarget': '0',
        'query.companySearchType': 'managers'
    }
    r = requests.get(
        'http://en.finder.fi/companysearch/search.fon',
        params=payload,
        headers=headers
    )

    # parse HTML response by BeautifulSoup
    soup = BeautifulSoup(r.text, 'html.parser')

    # find board members' links.
    for div_tag in soup.find_all(class_='itemName'):
        link = str(div_tag.a.get('href'))
        link = link.replace('contacts', 'management')
        replaced_link.append(link)

    return replaced_link


def _find_members(relative_path, searching_name):
    names = []
    print(relative_path)
    try:
        r = requests.get(
            'http://en.finder.fi' + relative_path,
            headers=headers
        )
        soup = BeautifulSoup(r.text, 'html.parser')
        div_tag = soup.find(id='list1')

        if div_tag is not None:

            for nested_div in div_tag.find_all(class_='resultdataHolderProfile'):

                if nested_div.p is not None:
                    names.append(nested_div.p.get_text())
                    print(names)

        if searching_name in names:

            return names

        else:
            matching = [
                single_name
                for single_name in names
                if searching_name in single_name
            ]

            return matching
    except UnicodeEncodeError:
        return None


def _find_companies_name(name):
    """Find companies' name."""
    companies_name = []

    # Set same charset as finder.fi's charset
    name = name.encode('iso-8859-15', 'ignore')

    payload = {
        'newSearch': 'true',
        'query.searchwords': name,
        'query.searchTarget': '0',
        'query.companySearchType': 'managers'
    }
    r = requests.get(
        'http://en.finder.fi/companysearch/search.fon',
        params=payload,
        headers=headers
    )

    # parse HTML response by BeautifulSoup
    soup = BeautifulSoup(r.text, 'html.parser')

    for link_tag in soup.find_all(class_='resultGrayplussa'):
        text = link_tag.get_text().replace('\n', '')
        text = text.strip()
        companies_name.append(text)

    for a_tag in soup.find_all(class_='resultGray'):
        text = a_tag.get_text().replace('\n', '')
        text = text.strip()
        companies_name.append(text)

    return companies_name
