from django.apps import AppConfig


class MainConfig(AppConfig):
    name = 'finder.apps.main'
