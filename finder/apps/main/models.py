from django.db import models


# Create your models here.
class Finder(models.Model):
    parent_name = models.TextField()
    parent_companies = models.TextField()
    child_names = models.TextField()
    created_at = models.DateTimeField(auto_now_add=True)
